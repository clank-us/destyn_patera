DestynPatera::Application.routes.draw do
  root to: 'pages#home'

  resources :categories, only: [:index, :create, :destroy]
  resources :videos
  resources :messages
end
