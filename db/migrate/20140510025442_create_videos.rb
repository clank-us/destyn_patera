class CreateVideos < ActiveRecord::Migration
  def change
    create_table :videos do |t|
      t.string     :title
      t.text :url
      t.text       :description
      t.date       :published_at
      t.boolean    :featured

      t.references :category

      t.timestamps

    end
  end
end
