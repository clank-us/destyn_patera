namespace :deploy do
  desc 'Deploy to staging'
  task :production do
    Paratrooper::Deploy.new('destyn-patera', tag: 'production').deploy
  end
end
