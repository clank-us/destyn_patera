class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  decent_configuration do
    strategy DecentExposure::StrongParametersStrategy
  end

  def authenticate
    authenticate_or_request_with_http_basic do |name, password|
      name == ENV["USER"] && password == ENV["PASSWORD"]
    end
  end

end
