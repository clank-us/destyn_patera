class MessagesController < ApplicationController

  before_filter :authenticate, except: :create

  expose(:messages)
  expose(:message, attributes: :message_params)

  def create
    if message.save
      redirect_to :root
    else
      render 'pages/home'
    end
  end

  private

  def message_params
    params.require(:message).permit(
      :name,
      :email,
      :phone,
      :subject,
      :body
    )
  end
end
