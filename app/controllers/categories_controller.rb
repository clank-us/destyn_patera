class CategoriesController < ApplicationController
  before_filter :authenticate

  expose(:categories)
  expose(:category, attributes: :category_params)

  def create
    if category.save
      redirect_to :categories
    else
      render :index
    end
  end

  def destroy
    category.destroy
    redirect_to :categories
  end


  private

  def category_params
    params.require(:category).permit(:name)
  end
end
