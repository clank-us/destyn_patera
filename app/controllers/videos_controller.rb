class VideosController < ApplicationController

  before_filter :authenticate

  expose(:categories)
  expose(:videos)
  expose(:video, attributes: :video_params)

  def create
    if video.save
      redirect_to :videos
    else
      render :new
    end
  end
  alias :update :create

  def destroy
    video.destroy
    redirect_to :videos
  end

  private

  def video_params
    params.require(:video).permit(
      :category_id,
      :title,
      :description,
      :url,
      :published_at,
      :featured,
      :thumbnail_url
    )
  end
end

