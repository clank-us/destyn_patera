class PagesController < ApplicationController
  expose(:categories) do
    Category.pluck(:name).map(&:titleize)
  end

  expose(:videos)
  expose(:message)
end
