module ApplicationHelper
  def featured_flag(video)
    "(Featured*)" if video.featured?
  end
end
