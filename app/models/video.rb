class Video < ActiveRecord::Base
  belongs_to :category

  validates :title, :url, :thumbnail_url, :category_id, :published_at, presence: true

  default_scope ->{ order(published_at: :desc).order(title: :asc) }
end
